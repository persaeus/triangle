def is_triangle(a, b, c):
    # Triangle inequality Theorem
    # Method 1
    # Check the three necessary inequalities
    return (a + b > c) and (b + c > a) and (c + a > b)
    # Return True if, and only if the three conditions are True
    # Method 2
    # return abs(a-b) > c < (a + b)
